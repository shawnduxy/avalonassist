const express = require('express')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const RedisStore = require('connect-redis')(session)

const redis = require('redis')
const client = redis.createClient({
  url: process.env.REDIS_URL
})

const wechat = require('wechat')
const webot = require('webot')

const log = require('debug')('avalon:log')
const verbose = require('debug')('avalon:verbose')
const error = require('debug')('avalon:error')

const schedule = require('node-schedule')

const utils = require('./lib/utils')
const clearRooms = require('./lib/storage').clearRooms

const config = {}
config.token = process.env.WX_TOKEN || 'token'
config.appid = process.env.WX_APPID || 'appid'
config.encodingAESKey = process.env.WX_AESKEY || 'aeskey'
config.checkSignature = process.env.WX_CHECK_SIGNATURE !== 'false'

client.on('error', err => {
  error('ERROR %O', err)
})

schedule.scheduleJob('*/5 * * * *', () => {
  clearRooms(client, (err, result) => {
    if (err) {
      error('failed to clear rooms: %o', err)
    } else {
      log('%d rooms were cleared', result)
    }
  })
})

const app = express()

app.use(cookieParser())
app.use(session({
  secret: 'avalonsecret',
  store: new RedisStore({
    client: client
  }),
  cookie: {
    maxAge: 3600000
  },
  resave: false,
  saveUninitialized: false
}))

require('./rules')(webot, client)

app.use('/avalon/images', express.static('images'))
app.use('/avalon', wechat(config, (req, res, next) => {
  let message = req.weixin
  log('receive message: %O', message)
  let info = {
    id: message.MsgId,
    uid: message.FromUserName,
    sp: message.ToUserName,
    createTime: message.CreateTime,
    type: message.MsgType,
    text: message.Content,
    session: req.wxsession
  }
  if (message.MsgType === 'event') {
    info.param = {
      event: message.Event
    }
  }
  webot.reply(info, (err, info) => {
    let response = utils.formatMessage(info)
    log('reply message: %O', response)
    res.reply(response)
    next()
  })
}))

let port = process.env.PORT || 3000;
app.listen(port, () => {
  log("Listening on %s", port)
})

app.enable('trust proxy')

if (!process.env.DEBUG) {
  console.log("set env variable `DEBUG=avalon:*` to display debug info.")
}
