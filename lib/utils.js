function formatMessage(info) {
  let reply = info.reply || ''
  let msgType = typeof reply !== 'object' ? 'text' : reply.type
  let content = reply.content || reply

  if (info.noReply) {
    content = ''
    msgType = 'text'
  }

  return {
    type: msgType,
    content: content
  }
}

module.exports = {
  formatMessage
}
