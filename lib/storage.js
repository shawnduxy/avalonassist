const _ = require('underscore')
const async = require('async')

const log = require('debug')('avalon:storage:log')
const verbose = require('debug')('avalon:storage:verbose')
const error = require('debug')('avalon:storage:error')

const gameSetup = [{
  totalPlayers: 5,
  good: 3,
  evil: 2,
  quests: []
}, {
  totalPlayers: 6,
  good: 4,
  evil: 2,
  quests: []
}, {
  totalPlayers: 7,
  good: 4,
  evil: 3,
  quests: []
}, {
  totalPlayers: 8,
  good: 5,
  evil: 3,
  quests: []
}, {
  totalPlayers: 9,
  good: 6,
  evil: 3,
  quests: []
}, {
  totalPlayers: 10,
  good: 6,
  evil: 4,
  quests: []
}]

//------------------------------------------------------------------------------
//    Rooms:
//      'roomCounts': Hashmap, every 100 rooms are grouped into one key,
//                    starting from 0 and than 100, 200, and so on, value is the
//                    number of rooms between [key, key + 100)
//      'rooms:<start>': Set, 'start' is the starting number of the rooms,
//                       every item in the set represents one room number
//      'room:<num>': Hashmap, setup for the room
//        Keys: 'gameVersion' - the game version
//              'totalPlayers' - the total number of players
//              'currentPlayerNum' - the current number of players
//              'good' - the number of good players
//              'evil' - the number of evil players
//              'playerToRole' - the JSON representing players to roles
//              'roleToPlayer' - the JSON representing roles to players
//              'players' - the JSON representing the players array
//              'currentQuest' - the current quest number
//              'currentQuestLeader' - the leader of current quest
//              'status' - the current status of the room
//                         0: created
//                         1: joining
//                         for X between 1 and 5
//                         X0: task X team selection phase
//                         X1: task X voting phase
//                         X2: task X task phase
//      'room:<num>:quest:<num>': Hashmap, information for a quest
//        Keys: 'result' - can be any of 'unknown', 'pass', or 'fail'
//              'leader' - the leader player index
//              'round' - the current round of the voting phase
//              'team' - the selected team
//              'votes' - comma separated voting result, each item can be either
//                        'unknown', 'approve', or 'reject'
//              'questVotes' - comma separated voting result for questing,
//                             each item can be either 'pass' or 'fail'
//              'status' - the current status of the task
//                         0 - team assembly
//                         1 - voting
//                         2 - questing
//                         3 - completed
//      'room:<num>:quest:<num>:round:<num>': Hashmap, information for a voting
//                                            voting round of one quest
//
//------------------------------------------------------------------------------

function clearRooms(client, next) {
  verbose('start clearing rooms')
  let total = 0
  async.waterfall([
    cb => {
      client.keys('rooms:*', cb)
    },
    (keys, cb) => {
      async.each(keys, (key, cb) => {
        log('checking room starting with %s', key)
        async.waterfall([
          cb => {
            client.smembers(key, cb)
          },
          (rooms, cb) => {
            log('found rooms %o', rooms)
            _.each(rooms, room => {
              log('checking room %s', room)
              async.waterfall([
                cb => {
                  client.exists(`room:${room}`, cb)
                },
                (exists, cb) => {
                  log('room %s exists: %d', room, exists)
                  if (exists === 0) {
                    log('clear room %s', room)
                    client.srem(`${key}`, `${room}`, cb)
                  } else {
                    cb(null, 0)
                  }
                },
                (result, cb) => {
                  total += result
                  if (result === 0) {
                    cb(null, 0)
                  } else {
                    log('decrease room count for %s by %d', key, result)
                    let start = key.substring(key.indexOf(':') + 1)
                    client.hincrby('roomCounts', `${start}`, -1 * result, cb)
                  }
                }
              ], err => {
                if (err) {
                  error('failed clear room %s because of %o', room, err)
                }
              })
            })
            cb()
          }
        ], cb)
      }, cb)
    }
  ], err => {
    next(err, total)
  })
}

function addRoom(client, next) {
  verbose('adding a new room')
  async.waterfall([
    cb => {
      client.hgetall('roomCounts', cb)
    },
    (roomCounts, cb) => {
      log('roomCounts: %O', roomCounts)
      let start = 0
      let max = 0
      if (roomCounts == null || roomCounts.length === 0) {
        start = 1
      } else {
        _.each(roomCounts, (value, key) => {
          let count = parseInt(value)
          let countStart = parseInt(key)
          if (start === 0 && count < 50) {
            start = countStart
          }
          max = Math.max(max, countStart)
        })
        if (start === 0) {
          start = countStart + 100
        }
      }
      cb(null, start)
    },
    (start, cb) => {
      log('room will be randomly created between %d and %d', start, start + 100)
      client.smembers(`rooms:${start}`, rooms => {
        cb(null, start, rooms)
      })
    },
    (start, rooms, cb) => {
      let room = Math.round(Math.random() * 100 + start)
      while (_.contains(rooms, room)) {
        room = Math.round(Math.random() * 100 + start)
      }
      log('new room number %d', room)
      cb(null, start, room)
    },
    (start, room, cb) => {
      client.sadd(`rooms:${start}`, room, result => {
        cb(null, result, start, room)
      })
    },
    (result, start, room, cb) => {
      if (result === 0) {
        cb('duplicate value')
      } else {
        cb(null, start, room)
      }
    },
    (start, room, cb) => {
      client.hincrby('roomCounts', start, 1, result => {
        cb(null, room)
      })
    }
  ], next)
}

function createGame(setup, room, client, next) {
  verbose('create new game version %d with %d players in room %d', setup.gameVersion, setup.totalPlayers, room)
  let playerRoles = assignPlayerRoles(setup.good, setup.evil)
  async.waterfall([
    cb => {
      let args = [`room:${room}`,
                  'gameVersion',
                  '' + setup.gameVersion,
                  'totalPlayers',
                  '' + setup.totalPlayers,
                  'currentPlayerNum',
                  '0',
                  'good',
                  '' + setup.good,
                  'evil',
                  '' + setup.evil,
                  'playerToRole',
                  JSON.stringify(playerRoles[0]),
                  'roleToPlayer',
                  JSON.stringify(playerRoles[1]),
                  'status',
                  '0']
      args.push(cb)
      client.hmset.apply(client, args)
    },
    (result, cb) => {
      client.expire(`room:${room}`, 3600, cb)
    },
    (result, cb) => {
      verbose('created room %s', room)
      cb(null, room, setup)
    }
  ], next)
}

function assignPlayerRoles(good, evil) {
  let players = []
  _.times(good, n => players.push('good' + n))
  _.times(evil, n => players.push('evil' + n))
  players = _.shuffle(players)
  verbose('the player roles are %O', players)
  let playerToRole = {}
  let roleToPlayer = {}
  _.each(players, (player, idx) => {
    playerToRole[`player${idx}`] = players[idx]
    roleToPlayer[players[idx]] = `player${idx}`
  })
  addAdditionalRoles(2, playerToRole, roleToPlayer)
  return [playerToRole, roleToPlayer]
}

function addAdditionalRoles(gameVersion, playerToRole, roleToPlayer) {
  switch (gameVersion) {
  case 1:
    break
  case 2:
    let merlin = roleToPlayer.good0
    roleToPlayer['merlin'] = roleToPlayer.good0
    delete roleToPlayer.good0
    playerToRole[merlin] = 'merlin'
    let assassin = roleToPlayer.evil0
    roleToPlayer['assassin'] = roleToPlayer.evil0
    delete roleToPlayer.evil0
    playerToRole[assassin] = 'assassin'
    break
  }
}

function startGame(gameVersion, totalPlayers, client, next) {
  verbose('start game version %d with %d players', gameVersion, totalPlayers)
  let setup = _.find(gameSetup, setup => setup.totalPlayers === totalPlayers)
  setup.gameVersion = gameVersion
  verbose('game setup is %O', setup)
  async.waterfall([
    cb => {
      addRoom(client, cb)
    },
    (room, cb) => {
      createGame(setup, room, client, cb)
    },
  ], next)
}

function enterRoom(info, room, client, next) {
  verbose('user %s wants to enter room %d', info.uid, room)
  async.waterfall([
    cb => {
      client.exists(`room:${room}`, cb)
    },
    (result, cb) => {
      if (result === 0) {
        cb('The room does not exist.')
      } else {
        cb()
      }
    },
    cb => {
      client.hget(`room:${room}`, 'status', cb)
    },
    (status, cb) => {
      if (status === '0' || status === '1') {
        cb()
      } else {
        cb('The game is already started.')
      }
    },
    cb => {
      client.hmget(`room:${room}`, 'totalPlayers', 'currentPlayerNum', 'playerToRole', 'players', 'roleToPlayer', cb)
    },
    (result, cb) => {
      let totalPlayers = parseInt(result[0])
      let currentPlayerNum = parseInt(result[1])
      let playerToRole = JSON.parse(result[2])
      let players = JSON.parse(result[3]) || []
      let roleToPlayer = JSON.parse(result[4])
      if (_.isUndefined(info.session.playerNum) && players.indexOf(info.uid) >= 0) {
        return cb('You are already in this room')
      }
      if (info.session.playerNum) {
        currentPlayerNum = info.session.playerNum - 1
      } else {
        players.push(info.uid)
      }
      let role = playerToRole['player' + currentPlayerNum]
      currentPlayerNum++
      let status = totalPlayers === currentPlayerNum ? '10' : '1'
      client.hmset(`room:${room}`, 'status', status, 'currentPlayerNum', '' + currentPlayerNum, 'players', JSON.stringify(players), (err, ignore) => {
        cb(err, currentPlayerNum, role, roleToPlayer)
      }),
      (currentPlayerNum, role, roleToPlayer, cb) => {
        client.expire(`room:${room}`, 3600, (err) => {
          cb(err, currentPlayerNum, role, roleToPlayer)
        })
      }
    },
    (currentPlayerNum, role, roleToPlayer, cb) => {
      let evilPlayers = getEvilPlayers(role, roleToPlayer)
      cb(null, currentPlayerNum, role, evilPlayers)
    }
  ], next);
}

function restartGame(info, client, next) {
  verbose('user %s wants to restart the current game in room %d', info.uid, info.session.room)
  async.waterfall([
    cb => {
      client.hmget(`room:${info.session.room}`, 'good', 'evil', cb)
    },
    (result, cb) => {
      let good = parseInt(result[0])
      let evil = parseInt(result[1])
      let playerRoles = assignPlayerRoles(good, evil)
      client.hmset(`room:${info.session.room}`, 'status', '1', 'playerToRole',
        JSON.stringify(playerRoles[0]), 'roleToPlayer', JSON.stringify(playerRoles[1]), (err) => {
          if (err) {
            cb(err)
          } else {
            cb(null, playerRoles[0], playerRoles[1])
          }
        })
    },
    (playerToRole, roleToPlayer, cb) => {
      client.expire(`room:${info.session.room}`, 3600, () => {
        let role = playerToRole['player' + (info.session.playerNum - 1)]
        cb(null, role, getEvilPlayers(role, roleToPlayer))
      })
    }
  ], next)
}

function getEvilPlayers(role, roleToPlayer) {
  let evilPlayers = []
  if (role.startsWith('evil') || role === 'assassin' || role === 'merlin') {
    _.each(roleToPlayer, (player, playerRole) => {
      if (playerRole.startsWith('evil') || playerRole === 'assassin') {
        evilPlayers.push(parseInt(player.substring(6)) + 1)
      }
    })
  }
  return evilPlayers
}

function getRoomInfo(uid, room, client, next) {
  verbose('user %s wants to get information for room %d', uid, room)
  async.waterfall([
    cb => {
      client.hmget(`room:${room}`, 'gameVersion', 'totalPlayers', 'good', 'evil', 'status', 'players', 'playerToRole', 'roleToPlayer', (err, result) => {
        if (err) {
          return cb(err)
        }
        let info = {
          gameVersion: parseInt(result[0]),
          totalPlayers: parseInt(result[1]),
          good: parseInt(result[2]),
          evil: parseInt(result[3]),
          status: parseInt(result[4])
        }
        let players = JSON.parse(result[5])
        let playerToRole = JSON.parse(result[6])
        let roleToPlayer = JSON.parse(result[7])
        let playerNum = players.indexOf(uid)
        info.role = playerToRole[`player${playerNum}`]
        info.evilPlayers = getEvilPlayers(info.role, roleToPlayer)
        cb(null, info)
      })
    },
    (info, cb) => {
      if (info.status >= 10) {
        let quest = info.status % 10
        //client.hmget(`romm:${room}:quest:${quest}`, )
      } else {
        cb(null, info)
      }
    }
  ], next)
}

module.exports = {
  clearRooms,
  startGame,
  enterRoom,
  restartGame,
  getRoomInfo
}
