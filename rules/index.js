const _ = require('underscore')

const log = require('debug')('avalon:log')
const verbose = require('debug')('avalon:verbose')
const error = require('debug')('avalon:error')

const storage = require('../lib/storage')

const roleNames = {
  'good1': 'Loyal Servant of Arthur',
  'good2': 'Loyal Servant of Arthur',
  'good3': 'Loyal Servant of Arthur',
  'good4': 'Loyal Servant of Arthur',
  'good5': 'Loyal Servant of Arthur',
  'merlin': 'Merlin',
  'evil1': 'Minion of Mordred',
  'evil2': 'Minion of Mordred',
  'evil3': 'Minion of Mordred',
  'assassin': 'Assassin',
  'percival': 'Percival',
  'mordred': 'Mordred',
  'morgana': 'Morgana',
  'oberon': 'Oberon'
}

function buildEvilPlayers(evilPlayers) {
  let response = ''
  _.each(evilPlayers, (evilPlayer, idx) => {
    if (idx !== 0) {
      response += ', '
    }
    if (idx === evilPlayers.length - 1 && response.length > 0) {
      response += 'and '
    }
    response += evilPlayer
  })
  return response
}

module.exports = function(webot, client) {
  webot.set({
    name: 'subscribe',
    description: 'new subscription',
    pattern: info => {
      return info.is('event') && info.param.event === 'subscribe'
    },
    handler: {
      type: 'text',
      content: [
        'Welcome to Avalon Assistant!',
        'We are still under development. And we expect to be fully functional by February 2017.'
      ].join('\n')
    }
  })

  /*
   * Start a new game. It will create an empty room with random room number.
   */
  webot.set({
    name: 'start',
    description: 'start a game',
    pattern: /start/i,
    handler: (info, next) => {
      info.wait('wait_player_num')
      //return next(null, 'Please choose:\n1 - The Resistance\n2 - The Resistance: Avalon')
      return next(null, 'Please choose number of players (5-10)')
    }
  })

  webot.waitRule('wait_game_version', (info, next) => {
    let n = parseInt(info.text)
    if (_.isNaN(n) || (n !== 1 && n !== 2)) {
      let response = 'Please choose:\n1 - The Resistance\n2 - The Resistance: Avalon'
      if (info.session.rewait_count <= 2) {
        response = 'Your input is not correct\n' + response
      }
      if (info.session.rewait_count === 3) {
        response = 'Let\'s start again\nReply "help" to get the description of valid commands'
        info.resolve()
        return next(null, response)
      }
      info.rewait()
      return next(null, response)
    }
    info.session.gameVersion = n
    info.wait('wait_player_num')
    return next(null, 'Please choose number of players (5-10)')
  })

  webot.waitRule('wait_player_num', (info, next) => {
    let n = parseInt(info.text)
    if (_.isNaN(n) || n < 5 || n > 10) {
      let response = 'Please choose number of players (5-10)'
      if (info.session.rewait_count <= 2) {
        response = 'Your input is not correct\n' + response
      }
      if (info.session.rewait_count === 3) {
        response = 'Let\'s start again\nReply "help" to get the description of valid commands'
        info.resolve()
        return next(null, response)
      }
      info.rewait()
      return next(null, response)
    }
    info.resolve()
    info.session.gameVersion = 2
    info.session.totalPlayers = n
    log('user %s is going to create a new room with version %d and %d players', info.uid, 2, n)
    storage.startGame(info.session.gameVersion, info.session.totalPlayers, client, (err, room, setup) => {
      info.session.createdRoom = room
      log('user %s created new room %s', info.uid, room)
      return next(null, `Room is ${room}\nThere are ${setup.good} good players and ${setup.evil} evil players\nPlease reply "enter" to enter the room`)
    })
  })

  /*
   * Enter an existing room. Every user is assigned a role.
   */
  webot.set({
    name: 'enter room',
    description: 'enter a room',
    pattern: /enter\s+(\d+)/i,
    handler: (info, next) => {
      let room = info.param['1']
      return enterRoom(info, room, client, next)
    }
  })

  webot.set({
    name: 'enter',
    description: 'enter a room',
    pattern: /enter/i,
    handler: (info, next) => {
      info.wait('enter_room')
      return next(null, 'Please enter the room number')
    }
  })

  webot.waitRule('enter_room', (info, next) => {
    let room = parseInt(info.text)
    if (_.isNaN(room)) {
      let response = 'Please enter the room number'
      if (info.session.rewait_count <= 2) {
        response = 'Your input is not correct\n' + response
      }
      if (info.session.rewait_count === 3) {
        response = 'Let\' start again\nReply "help" to get the description of valid commands'
        info.resolve()
        return next(null, response)
      }
      info.rewait()
      return next(null, response)
    }
    return enterRoom(info, room, client, next)
  })

  function enterRoom(info, room, client, next) {
    storage.enterRoom(info, room, client, (err, num, role, evilPlayers) => {
      if (err) {
        return next(null, err)
      }
      info.session.room = room
      info.session.role = role
      info.session.playerNum = num
      let response = `You are now in room ${room}\nYou are player number ${num}\nYour role is ${roleNames[role]}`
      if (evilPlayers.length !== 0) {
        response += '\nEvil players are player ' + buildEvilPlayers(evilPlayers)
      }
      return next(null, response)
    })
  }

  webot.set({
    name: 'new',
    description: 'start new game',
    pattern: /new/i,
    handler: (info, next) => {
      if (_.isUndefined(info.session.createdRoom) && _.isUndefined(info.session.room)) {
        return next(null, 'You are not part of any room')
      }
      if (info.session.createdRoom) {
        return storage.restartGame(info, client, (err, role, evilPlayers) => {
          let response = `You are now in room ${info.session.room}\nYou are player number ${info.session.playerNum}\nYour role is ${roleNames[role]}`
          if (evilPlayers.length !== 0) {
            response += '\nEvil players are player ' + buildEvilPlayers(evilPlayers)
          }
          return next(null, response)
        })
      }
      return enterRoom(info, info.session.room, client, next)
    }
  })

  webot.set({
    name: 'help',
    description: 'display help',
    pattern: /help/i,
    handler: (info, next) => {
      let response = ''
      if (_.isUndefined(info.session.createdRoom) && _.isUndefined(info.session.room)) {
        return next(null, 'You are not part of any room, please use the following commands:\n"create": to create a new room\n"enter": to enter an existing room')
      }
      if (!_.isUndefined(info.session.room)) {
        return storage.getRoomInfo(info.uid, info.session.room, client, (err, result) => {
          if (err) {
            return next(null, err)
          }
          let response = `You are currently in room ${info.session.room}\nThere are ${result.totalPlayers} players, ${result.good} are good and ${result.evil} are evil\n`
          response += `Your role is ${roleNames[result.role]}`
          if (result.evilPlayers.length !== 0) {
            response += '\nEvil players are player ' + buildEvilPlayers(result.evilPlayers)
          }
          return next(null, response)
        })
      }
      if (!_.isUndefined(info.session.createdRoom)) {
        return next(null, `You created room ${info.session.createdRoom}\nPlease ask all players (including yourself) to reply "enter" to join the game`)
      }
    }
  })

/*
  webot.set({
    name: 'setup',
    description: 'set up an existing game',
    pattern: /setup/i,
    handler: (info, next) => {
      if (_.isUndefined(info.session.createdRoom)) {
        return next(null, 'You did not create any room')
      }
    }
  })
*/

  webot.set(/.*/, (info, next) => {
    verbose('unhandled message: %s', info.text)
    return next(null, 'This is help')
  })
}
